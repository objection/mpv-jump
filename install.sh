#!/usr/bin/env sh

if [[ $EUID == 0 ]]; then
	echo "${0:t}: Don't install this as root; or if you want to, modify this script" >&2
	exit 1
fi
gcc -g -std=c2x -o mpv-jump.so src/main.c $(pkg-config --cflags mpv) -shared -fPIC ||
	exit 1

config_dir=$HOME/.config/mpv/scripts

# Not dealing with MPV's files, like MPV_HOME, because the MPV
# documentation seems a bit ambiguous.
if [[ ! -d $config_dir ]]; then
	echo "${0:t}: You don't have an mpv config directory"
	exit 1
fi
mv mpv-jump.so $config_dir
