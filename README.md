# MPV Jump

This is a "jumplist", like you get in Vim. Big "jumps" -- seeks -- will
be recorded. You can go back and forward through them by setting up
bindings.

It works like this: You open a file you haven't opened before. You'll
have one jump, at 0 seconds. You do a seek of some duration; now you
have two jumps, at 0 seconds, and, say, 10 minutes. Make another one,
why don't you, to 20 minutes. Now you can go back through the
jumplist, to 10 minutes, and then to 0 minutes. You can also go
forward through the jumplist, back to 20 minutes.

It should be relatively easy to understand when you use it, since a
little map gets printed, which might look like this:

```txt
 mpv-jump-history 0: 0
[mpv-jump-history 1: 3840]
 mpv-jump-history 2: 1000
```

In this case, you are on the second one. Those values are in seconds.
Later, I guess I'll use proper duration strings.

You'll need to set three mappings. I've used ctrl-o for back, and tab
(ctrl-i in the terminal) for forward. Those are the Vim mappings. I've
done ctrl-p for "print".

```conf
ctrl+o script_message mpv_jump back
tab script_message mpv_jump forwards
ctrl+p script_message mpv_jump print-jumps
ctrl+l script_message mpv_jump last
```

Your jump history will be written out into the directory the file is
in. Its name will be a dot, then mpv-jumps, then the name of the file
without its suffix. Eg, say the full path of the file you're playing
is `/home/me/my-audiobooks/my-file.m4b`. The history file will be
`/home/me/my-audiobooks/.mpv-jump-my-file`

This program only works on individual files. It can't give you a
jumplist for playlists or directories. Maybe in the future.

My primary use for this plugin is to help me find my place in
audiobooks I've fallen asleep while listening to. If you think this
plugin might be useful, you'll need to convert your split-up
audiobooks into one file.

Note that we don't save your position on exit. MPV can do that itself,
when you quit with q, and when you use the --save-position-on-quit
flag. The point of this plugin isn't to remember where you exited.
It's to remember where you were when you started.

