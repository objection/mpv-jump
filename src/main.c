#define _GNU_SOURCE

#include <stdio.h>
#include <assert.h>
#include <ctype.h>
#include <toml.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <errno.h>
#include <limits.h>
#include <libgen.h>
#include <stdlib.h>
#include <stdbool.h>
#include <mpv/client.h>

// This script is a little mucky because it's not easy to tell when a
// seek really starts and ends.
//
// This is because, though MPV gives plugins the MPV_EVENT_SEEK event,
// and the "seek" property, neither of these can be used as-is to
// detect a long seek -- ie, one where you'd holding the key down,
// because they don't fire just once, when you hit the key; they fire
// once for every key-repeat.
//
// I think the OSD works differently. MPV is able to do its own input
// processing. When it's doing that it can detect keydown and keyup
// events.
//
// But I can't use that, since I want things to work in the
// terminal. Hence this script becomes mucky. I'm using timings to
// detect the starts and ends of seeks.

#define $goto(_where) ({ goto _where; })
enum { JUMPLIST_LENGTH = 100 };
enum { SEEK_SECS_BEFORE_RECORD = 60 * 10 };

// It's like this: When you seek, this script can't necessarily
// tell if you sought just once or for 10 minutes, reason being, when
// you hold down the key, you don't get SEEK but SEEK PLAYBACK_RESTART
// SEEK PLAYBACK_RESTART. That is until you've held the key for like
// two seconds, at which point it's just SEEEEEEEEEEEEEEEEEEEK.
//
// So in order to know if you've released the key we simply need to
// wait. This number is just a number that works. Better solutions?
// I can't see a way to tell to get keydown status from MPV. MPV, I
// swear, _can_ detect it, even in the terminal. I say this, because
// the initial SEEKs come way faster than my keyboard repeat. But
// maybe MPV isn't detecting keydown but just using a timer like me.
/* #define SEEK_COOLDOWN 25000 + 15000 */
enum { SEEK_COOLDOWN = 25000 + 15000 };
enum { LINE_LENGTH = 60 * 5 };
#define $err_msg_no_newline(fmt, ...) \
	do { \
		/* An, I think futile, attempt to make sure stuff outputs */ \
		/* properly.  */ \
		fflush (stdout); \
		fprintf (stderr, "mpv-jumps: " fmt, ##__VA_ARGS__); \
		fflush (stderr); \
	} while (0)
#define $err_msg(fmt, ...) \
	do { \
		$err_msg_no_newline(fmt, ##__VA_ARGS__); \
		fputc ('\n', stderr); \
	} while (0)

struct jumplist { int64_t *d; size_t n, length, pos; };

struct file_details {
	char *path, *dir;
	char *jumplist_path;
	bool failed;
};

struct hms {
	int hours, minutes, seconds;
};
#define $date_fmt_str "%.2d:%.2d:%.2d"
#define $hms_members(_it) _it.hours, _it.minutes, _it.seconds

static struct jumplist jumplist;
static mpv_handle *handle;

static void free_file_details (struct file_details *it) {
	if (it->path) mpv_free (it->path);
	if (it->jumplist_path) free (it->jumplist_path);
}

// I could actually get this info from MPV by asking for the string
// version of the property "playback-time".
struct hms secs2hms (int64_t seconds) {
	return (struct hms) {
		.hours = seconds / 3600,
		.minutes = (seconds % 3600) / 60,
		.seconds = seconds % 60,
	};
}

static void ctrl_p (void) {
	if (!jumplist.n) {
		$err_msg ("No jumps");
		return ;
	}

	// Without this newline stuff will print on the last line
	// that MPV printed. I know why. It's to do with how they're
	// making that line keep bring reprinted on the same line.
	putchar ('\n');
	for (int i = 0; i < jumplist.n; i++) {
		printf ("%smpv-jump: jump %d: " $date_fmt_str "\n",
				i == jumplist.pos ? "> " : "  ",
				i, $hms_members (secs2hms (jumplist.d[i])));
	}
	if (jumplist.pos == jumplist.n)
		printf (">\n");
}

static int remove_from_jumplist (int idx) {
	size_t copy_len = jumplist.d + jumplist.length - (jumplist.d + idx );
	memcpy (jumplist.d + idx, jumplist.d + idx + 1, sizeof *jumplist.d * copy_len);
	jumplist.n--;
	if (jumplist.pos > idx)
		jumplist.pos = idx;
	return 0;
}

static int add_to_jumplist (int64_t new_val) {
	if (jumplist.n == jumplist.length - 1) {
		size_t copy_len = jumplist.length - 1;
		memcpy (jumplist.d, jumplist.d + 1, sizeof *jumplist.d * copy_len);
		jumplist.d[jumplist.n] = new_val;
	} else
		jumplist.d[jumplist.n++] = new_val;
	return 0;
}

static int64_t get_playback_time (void) {

	uint64_t r;
	int rt = mpv_get_property (handle, "playback-time", MPV_FORMAT_INT64, &r);
	if (rt) {
		$err_msg ("Couldn't get playback-time: %s", mpv_error_string (rt));
		return -1;
	}
	return r;
}

static int seek_to_jumplist_pos () {
	char pos_secs_str[64];
	snprintf (pos_secs_str, 64, "%ld", jumplist.d[jumplist.pos]);
	int rt = mpv_command (handle, (const char *[]) {"seek", pos_secs_str,
			"absolute", 0});
	if (rt) {
		$err_msg ("Failed to seek to " $date_fmt_str ": %s",
				$hms_members (secs2hms (jumplist.d[jumplist.pos])),
				mpv_error_string (rt));
		return 1;
	}
	return 0;
}

static int do_input (struct mpv_event *event) {
	struct mpv_event_client_message *msg = event->data;
	if (msg->num_args != 2) {
		$err_msg_no_newline ("input.conf line \"");
		for (int i = 0; i < msg->num_args; i++)
			fprintf (stderr, "%s%s", msg->args[i], i != msg->num_args - 1 ? " " : "");
		fprintf (stderr, "\
\" should just two args, \"mpv-jumps\" and then \"backwards\", \"forwards\" or \
\"print-jumps\"\n");
		return 1;
	}

	time_t time_pos = get_playback_time ();

	switch (msg->args[1][0]) {
		case 'l':
			seek_to_jumplist_pos ();
			ctrl_p ();
			break;
		case 'b': {
			if (jumplist.pos == 0)
				break;
			/* int64_t pos_seconds = jumplist.d[jumplist.pos]; */

			if (jumplist.pos == jumplist.n) {
				if (time_pos < jumplist.d[jumplist.pos] - LINE_LENGTH ||
						time_pos > jumplist.d[jumplist.pos] + LINE_LENGTH)
					add_to_jumplist (time_pos);
			}

			jumplist.pos--;

			seek_to_jumplist_pos ();

			ctrl_p ();
		}
			break;
		case 'f':
			if (jumplist.pos < jumplist.n - 1)
				jumplist.pos++;
			seek_to_jumplist_pos ();
			ctrl_p ();
			break;
		case 'p':
			ctrl_p ();
			return 0;
		default:
			$err_msg ("\
Bad argument \"%s\" in your input.conf mpv_jump line. Valid arguments are \"backwards\", \
\"forwards\", and \"print-jumps\"", msg->args[1]);
			return 1;
	}

	ctrl_p ();

	return 0;
}

// Not static because it's not used and I don't want complaints. I
// could use GNU attributes or the new C ones, but I don't want the
// user (me) to use GNU or new C.
char *get_line_from_cmd (char *cmd) {
	FILE *pipe = popen (cmd, "r");

	size_t n_line = 0;
	char *r = 0;
	size_t rt = getline (&r, &n_line, pipe);
	if (rt == -1)
		return 0;
	rt = pclose (pipe);

	// Not really indicating that pclose can fail.
	if (rt) {
		if (r) free (r);
		return 0;
	}
	return r;
}

static char *get_file_path (void) {
	char *r = 0;
	int rt = mpv_get_property (handle, "path", MPV_FORMAT_STRING, &r);
	if (rt) {
		$err_msg ("Couldn't get filename: %s", mpv_error_string (rt));
		return 0;
	}
	return r;
}

static int write_jumplist_file (struct file_details file_details) {

	// There should always be at least one entry; 0.
	if (!jumplist.n) {
		$err_msg ("Jumplist is empty; this is a bug");
		return 1;
	}

	FILE *f = fopen (file_details.jumplist_path, "w+");
	if (!f) {

		// It's not an error to not be able to open the file for
		// writing. That seems fine.
		$err_msg ("\
Couldn't open jumplistfile %s for writing. Jumps will not be saved: %s",
file_details.jumplist_path,
strerror (errno));
		return 1;
	}

	for (int i = 0; i < jumplist.n; i++) {
		if (jumplist.pos == i)
			fprintf (f, "* ");
		fprintf (f, "%ld\n", jumplist.d[i]);
	}
	if (fclose (f)) {
		$err_msg ("\
Couldn't close jumplist file %s. Not exiting, though potentially it's empty now",
file_details.jumplist_path);
		return 1;
	}

	// Note: If your pos is at the end of the list we just don't note
	// it down.

	return 0;
}

static bool is_all_space (char *b) {
	while (*b) { if (!isspace (*b)) return 0; b++; }
	return 1;
}

static int read_jumplist_file (char *filename) {
	int r = 1;
	FILE *f = fopen (filename, "r");
	if (!f)
		return r;
	size_t n = 0;
	char *line = 0;
	int rt = 0;
	jumplist.length = JUMPLIST_LENGTH;
	jumplist = (struct jumplist) {.length = JUMPLIST_LENGTH,
		.d = calloc (sizeof *jumplist.d, JUMPLIST_LENGTH), .pos = -1};
	assert (jumplist.d);
	char *p;
#define $read_jumplist_file_err(fmt, ...) \
	$err_msg ("%s:%ld: " fmt "; exiting MPV Jumps", filename, jumplist.n, ##__VA_ARGS__);

	while ((rt = getline (&line, &n, f) != -1)) {
		assert (jumplist.n < JUMPLIST_LENGTH);
		char *newline = strchr (line, '\n');
		if (newline) *newline = 0;
		if (is_all_space (line))
			continue;
		p = line;
		while (isspace (*p)) p++;
		if (*p == '*') {
			if (jumplist.pos != -1) {
				$read_jumplist_file_err ("You've got too many position markers (*)");
				goto out;
			}
			jumplist.pos = jumplist.n;
			p++;
			while (isspace (*p)) p++;
		}
		int64_t seconds = 0;
		int rt = sscanf (p, "%ld", &seconds);
		if (rt != 1) {
			$read_jumplist_file_err ("Couldn't get number from \"%s\"", line);
			goto out;
		}
		jumplist.d[jumplist.n++] = seconds;
	}

	// If no jumplist was recorded it means you with the your
	// jumplist position off its end -- which is the normal case.
	if (jumplist.pos == -1)
		jumplist.pos = jumplist.n;

	if (rt == -1) {
		$read_jumplist_file_err ("Failed to read jumplist file");
		goto out;
	}

	if (!jumplist.n) {
		$read_jumplist_file_err ("File is empty");
		goto out;
	}
	if (jumplist.pos == -1) {
		$read_jumplist_file_err ("No pos marker");
		goto out;
	}
	r = 0;
out:
	if (f && fclose (f)) {
		$err_msg ("\
Couldn't close jumplist file %s opened for reading. I won't try writing", filename);
		r = 1;
	}
	if (line) free (line);
	return r;
}

static int init_or_reinit_jumplist () {
	if (!jumplist.d)
		jumplist = (struct jumplist) {.length = JUMPLIST_LENGTH,
			.d = calloc (sizeof *jumplist.d, JUMPLIST_LENGTH), .n = 1};

	if (!jumplist.d) {
		$err_msg ("Out of memory, probably");
		return 1;
	}

	jumplist.d[0] = 0;
	return 0;
}

// Badly-named.
static struct file_details init_file_details (void) {
	struct file_details r = {
		.path = get_file_path (),
		.failed = 1,
	};
	if (!r.path)
		return r;

	char tmp_path[PATH_MAX];
	strncpy (tmp_path, r.path, PATH_MAX - 1);
	char *percent_t = basename (tmp_path);

	char dir[PATH_MAX];
	strncpy (dir, dirname (tmp_path), PATH_MAX - 1);
	char *suffix = strrchr (percent_t, '.');
	if (suffix) *suffix = 0;

	asprintf (&r.jumplist_path, "%s/.%s-mpv-jump", dir, percent_t);

	int rt = access (r.jumplist_path, F_OK);

	// If we're able to read the jumplist file, we use it, else we just
	// init jumplist to 0. Not getting the jumplist file read isn't an
	// error.
	if (!rt) {
		!read_jumplist_file (r.jumplist_path) ?: ({
			goto out;
		});

	} else
		init_or_reinit_jumplist ();

	r.failed = 0;
out:
	return r;
}

static int doit (void) {

	int r = 1;
	int64_t full_seek_start = 0;
	bool got_playback_restart = 0;
	int64_t current_μs = -1, this_seek_end_μs = 0;
	bool seeking = 0;
	int64_t current_playback_time = 0;

	struct file_details file_details = init_file_details ();
	if (file_details.failed)
		goto out;
	ctrl_p ();

    while (1) {

		// Can't have an infinite wait, as far as I can see, since we
		// want to be able to get a reasonably up-to-date
		// current_playback_time. An alternative way would be to get
		// it on AUDIO_RECONFIG VIDEO_RECONFIG, since those happen
		// before END. But do they always? It's a bit fragile, so
		// let's just wait more often.
        mpv_event *event = mpv_wait_event (handle, 1);
		// event can never be 0.

		/* const char *event_name = mpv_event_name (event->event_id); */
		/* fprintf (stdout, "Got event: %d, %s\n", event->event_id, event_name); */

		current_μs = mpv_get_time_us (handle);

		// Need to get this here, since we want it for END, and
		// when that happens, you can't get the time.
		int64_t it = get_playback_time ();
		if (it != -1)
			current_playback_time = it;

		if (got_playback_restart
				&& current_μs - this_seek_end_μs > SEEK_COOLDOWN) {
			got_playback_restart = 0;
			seeking = 0;

			if (current_playback_time == -1)
				$err_msg ("Couldn't get playback-time. Not adding jump");
			else {

				int64_t dur = llabs (current_playback_time - full_seek_start);
				if (dur > SEEK_SECS_BEFORE_RECORD) {

		// From the Vim help.
		// 		If you use a jump command, the current line number is
		// 		inserted at the end of the jump list. If the same line
		// 		was already in the jump list, it is removed.
		// A seek is a a jump. A ctrl-o is a jump too, if you were off
		// the end of the jumplist when you did it.
					for (int i = 0; i < jumplist.n; i++) {

		// We remove the "lines" that where there. In Vim, a line is a
		// line. In this, a line is like five minutes or something.
		// I'll figure out the right duration.
						if (full_seek_start > jumplist.d[i] - LINE_LENGTH &&
								full_seek_start < jumplist.d[i] + LINE_LENGTH) {
							remove_from_jumplist (i);
						}
					}
					add_to_jumplist (full_seek_start);
					jumplist.pos = jumplist.n;
				}
			}
		}

		switch (event->event_id) {
			case MPV_EVENT_SHUTDOWN:
				// fallthrough
			case MPV_EVENT_END_FILE: {

				// Add a final jump. MPV has its
				// --save-position-on-quit flag, but this plugin
				// doesn't know about that, so it won't become part of
				// the jumplist.
				add_to_jumplist (current_playback_time);
				if (!write_jumplist_file (file_details))
					r = 0;
				goto out;
			}
			break;

			break;
			case MPV_EVENT_FILE_LOADED:
				/* assert (!"MPV_EVENT_FILE_LOADED; Should be impossible"); */
			break;

			case MPV_EVENT_CLIENT_MESSAGE:
				do_input (event);
				break;
			case MPV_EVENT_SEEK:
				if (!seeking) {

					// This isn't perfect. Event thought client.h
					// says "Happens when a seek was initiated."
					// time two minutes past the actual seek. I don't
					// want to hard-code that 2 minutes,
					full_seek_start = get_playback_time ();
					seeking = 1;
				}
				break;
			case MPV_EVENT_PLAYBACK_RESTART:
				if (seeking) {
					got_playback_restart = 1;
					this_seek_end_μs = mpv_get_time_us (handle);
				}
				break;
			default:
				break;

		}
    }
out:
	free_file_details (&file_details);
    return r;
}

int mpv_open_cplugin (mpv_handle *_handle) {
	handle = _handle;

	/* repeat_rate = get_repeat_rate (); */

	while (1) {
		mpv_event *event = mpv_wait_event (handle, -1);
		switch (event->event_id) {
			case MPV_EVENT_SHUTDOWN:
				goto out;
			case MPV_EVENT_FILE_LOADED:

				!doit () ?: $goto (out);
				break;
			default:
				break;
		}
	}
out:;
	return 0;
}


