#!/usr/bin/env bash

shopt -s globstar

gcc -M src/*.[ch] | sed -e 's/[\\ ]/\n/g' | \
        sed -e '/^$/d' -e '/\.o:[ \t]*$/d' | \
		ctags --extras=+q -L -

